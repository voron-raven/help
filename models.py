import datetime
from bcrypt import hashpw
import jwt

from peewee import (Model, CharField, TextField, DateTimeField,
                    SmallIntegerField, ForeignKeyField)
from peewee_async import PostgresqlDatabase
from playhouse.postgres_ext import IntervalField

from sanic.exceptions import abort

import settings

database = PostgresqlDatabase(**settings.DB_CONFIG)


async def authenticate(title: str, password: str):
    """ If the given credentials are valid, return a Organization object. """
    organization = Organization.get_or_none(title=title)
    if organization and organization.password == hash_password(password):
        return organization


def get_token(payload: dict) -> str:
    """ Store organization id and title in a token. """
    return jwt.encode(
        payload,
        settings.SECRET_KEY,
        algorithm='HS256'
    ).decode("utf-8")


def get_payload(token: str) -> dict:
    """ Get organization id and title from the token. """
    try:
        return jwt.decode(token, settings.SECRET_KEY, algorithms=['HS256'])
    except jwt.DecodeError as e:
        abort(400, message=str(e))


def logout(request):
    """ Remove authentication token from headers. """
    return request['headers'].pop('authentication', None)


def hash_password(value: str) -> str:
    """ Hash user password to store hash in a database. """
    return hashpw(value.encode('utf-8'), settings.SECRET_KEY.encode(
        'utf-8')).decode("utf-8")


class PasswordField(CharField):
    def db_value(self, value):
        """Convert the python value for storage in the database."""
        return hash_password(value)


class Organization(Model):
    """ Organization model. """

    logo = CharField(unique=True)
    title = CharField(unique=True)
    site = CharField(unique=True)
    info = TextField()
    password = PasswordField()

    class Meta:
        database = database


class Opportunity(Model):
    """ Opportunity model. """

    title = CharField()
    organization = ForeignKeyField(Organization)
    task = TextField()
    category = SmallIntegerField(choices=(
        (0, "Writing and editing"),
        (1, "Translation"),
        (2, "Art and design"),
        (3, "Technology development"),
        (4, "Research"),
        (5, "Outreach and advocacy"),
        (6, "Project development and management"),
        (7, "Leadership & strategy"),
        (8, "Teaching and training"),
        (9, "Administration"),
        (10, "Community organizing"),
        (11, "Volunteer management"),
        (12, "Event organization"),
        (13, "Healthcare services"),
        (14, "Manual labor & skilled trade"),
    ))
    area_of_expertise = SmallIntegerField(null=True, choices=())
    objectives = TextField()
    vol_needed = SmallIntegerField()
    hours_per_week_min = SmallIntegerField()
    hours_per_week_max = SmallIntegerField()
    duration = IntervalField()
    requirements = TextField()
    region = SmallIntegerField(choices=(
        (0, "Global"),
        (1, "Africa"),
        (2, "Asia"),
        (3, "Europe"),
        (4, "Latin America and the Caribbean"),
        (5, "Northern America"),
        (6, "Oceania"),
    ))
    country = SmallIntegerField(null=True, choices=())
    language = SmallIntegerField(choices=(
        (0, "Ukrainian"),
        (1, "English"),
        (2, "French"),
        (3, "Spanish"),
    ))
    labels = SmallIntegerField(null=True, choices=())
    posted = DateTimeField(default=datetime.datetime.now)

    class Meta:
        database = database


MODELS = (Organization, Opportunity)
