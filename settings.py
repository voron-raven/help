import os
import requests

SITE_ENV_PREFIX = 'HELP'


def get_env_var(name: str, default: str = '') -> str:
    """ Get all sensitive data from google vm custom metadata. """
    try:
        name = '_'.join([SITE_ENV_PREFIX, name])
        res = os.environ.get(name)
        if res:
            # Check env variable (Jenkins build).
            return res
        else:
            res = requests.get(
                'http://metadata.google.internal/computeMetadata/'
                'v1/instance/attributes/{}'.format(name),
                headers={'Metadata-Flavor': 'Google'}
            )
            if res.status_code == 200:
                return res.text
    except requests.exceptions.ConnectionError:
        pass

    return default


SECRET_KEY = get_env_var('SECRET_KEY', '$2b$12$Z1few6/MTKVXcBNXKxRUGu')


DB_CONFIG = {
    'user': get_env_var('DB_USER', 'help_admin'),
    'password': get_env_var('DB_PASSWORD', 'help_admin_JqPWQ73>'),
    'host': get_env_var('DB_HOST', '127.0.0.1'),
    'database': get_env_var('DB_NAME', 'help'),
    'autocommit': True,
    'autorollback': True,
}
