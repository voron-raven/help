from functools import wraps

from peewee import DatabaseError
from playhouse.shortcuts import dict_to_model

from sanic.exceptions import abort
from sanic.views import HTTPMethodView

from app import app, json_response
from models import Opportunity, get_token, authenticate, get_payload


def authorized(methods=('POST', 'PUT', 'DELETE')):
    """
    Decorator to check authorization
    and get organization by authorization header.
    """
    def decorator(f):
        @wraps(f)
        async def decorated_function(request, *args, **kwargs):
            if request.method not in methods:
                return await f(request, *args, **kwargs)

            # get organization info from authorization header
            payload = get_payload(request.headers.get('authorization',  {}))

            if payload and set(payload.keys()) == {'id', 'title'}:
                request['organization'] = payload
                # the organization is authorized.
                # run the handler method and return the response
                return await f(request, *args, **kwargs)
            else:
                # the organization is not authorized.
                abort(403,
                      message="This action requires authorization header")
        return decorated_function
    return decorator


class OpportunitiesView(HTTPMethodView):
    decorators = [authorized()]

    # noinspection PyMethodMayBeStatic
    async def get(self, _):
        """ List of opportunities. """
        opportunities = Opportunity.select()
        return json_response(opportunities, 200)

    # noinspection PyMethodMayBeStatic
    async def post(self, request):
        """ Create a new opportunity. """
        opportunity = {}
        try:
            data = request.json
            data.pop('id', None)  # remove id
            opportunity = dict_to_model(Opportunity, data)
            opportunity.organization = request['organization']['id']
            opportunity.save()
        except (DatabaseError, ValueError) as e:
            abort(400, message=str(e))

        return json_response(opportunity, 201)


class OpportunityView(HTTPMethodView):
    decorators = [authorized()]

    # noinspection PyMethodMayBeStatic
    async def get(self, _, pk: int):
        """ Get opportunity by id. """
        opportunity = Opportunity.select().where(Opportunity.id == pk).first()
        return json_response(opportunity, 200)

    # noinspection PyMethodMayBeStatic
    async def put(self, request, pk: int):
        """ Edit opportunity. """
        try:
            # Allow editing only for the owner.
            rows_edited = Opportunity.update(**request.json).where(
                Opportunity.id == pk,
                Opportunity.organization == request['organization']['id']
            ).execute()

            if rows_edited:
                return json_response(dict_to_model(Opportunity, request.json),
                                     200)

        except (DatabaseError, ValueError) as e:
            abort(400, message=str(e))

        return json_response({}, 404)

    # noinspection PyMethodMayBeStatic
    async def delete(self, request, pk: int):
        """ Delete opportunity. """
        # Allow deleting only for the owner.
        rows_deleted = Opportunity.delete().where(
            Opportunity.id == pk,
            Opportunity.organization == request['organization']['id']
        ).execute()

        return json_response({}, 204 if rows_deleted else 404)


class LoginView(HTTPMethodView):

    # noinspection PyMethodMayBeStatic
    async def post(self, request):
        """ Login. """
        if 'title' in request.json and 'password' in request.json:
            organization = await authenticate(request.json['title'],
                                              request.json['password'])
            if organization:
                token = get_token(
                    {'id': organization.id, 'title': organization.title}
                )
                return json_response({'authorization': token}, 201)
            else:
                abort(400, message="Not valid title or password!")

        abort(400, message="Need to specify name and password!")


app.add_route(OpportunitiesView.as_view(), '/', version=1)
app.add_route(OpportunityView.as_view(), '/<pk:int>', version=1)
app.add_route(LoginView.as_view(), '/login', version=1)


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8000)
