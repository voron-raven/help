import datetime
import json

from peewee import Model, Select
from playhouse.shortcuts import model_to_dict

from sanic import Sanic
from sanic_cors import CORS
from sanic.response import json as sanic_json
from sanic.exceptions import SanicException

import settings

app = Sanic()
app.config['SECRET_KEY'] = settings.SECRET_KEY
CORS(app)


class DBModelEncoder(json.JSONEncoder):
    """ Class to turn all DB models into JSON objects. """

    def default(self, obj):
        if isinstance(obj, (datetime.datetime, datetime.timedelta)):
            return str(obj)

        if isinstance(obj, Select):
            return list(obj)

        if isinstance(obj, Model):
            return model_to_dict(obj)

        # Everything else
        return super().default(obj)


def json_response(data, code: int = 200, headers: dict = None):
    """ Format json response. """

    return sanic_json(data, status=code, headers=headers, dumps=json.dumps,
                      cls=DBModelEncoder)


@app.exception(SanicException)
async def exception_handler(_, exception: SanicException) -> tuple:
    """ Exception handler returns error in json format. """
    return json_response({"message": exception.args}, exception.status_code)
