from setuptools import setup, find_packages

setup(
    name='help-api',
    version='0.0.2',
    packages=find_packages(),
    install_requires=[
        'requests==2.21.0',
        'aiohttp==3.4.4',
        'sanic==0.8.3',
        'Sanic-Cors==0.9.7',
        'aiopg==0.15.0',
        'psycopg2==2.7.6.1',
        'psycopg2-binary==2.7.6.1',
        'peewee==3.7.1',
        'peewee-async==0.6.0a0',
        'bcrypt==3.1.5',
        'PyJWT==1.7.1',
    ],
    extras_require={
        "test": [
            'coverage==4.5.1',
        ]
    },
)
