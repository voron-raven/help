import bcrypt

from peewee import PostgresqlDatabase
from playhouse.migrate import PostgresqlMigrator, migrate

import settings
from models import Organization, PasswordField, hash_password

database = PostgresqlDatabase(**settings.DB_CONFIG)
migrator = PostgresqlMigrator(database)
table = Organization._meta.table.__name__


# Add password column to organization.
migrate(
    migrator.add_column(table, 'password', PasswordField(null=True)),
)

# Set random password.
for organization in Organization.select():
    organization.password = hash_password(bcrypt.gensalt().decode("utf-8"))
    organization.save()

# Set not NULL for password column.
migrate(
    migrator.add_not_null(table, 'password'),
)
