import re
import os

from peewee import ProgrammingError

from models import MODELS


def get_version():
    setup_file = os.path.join('..', 'setup.py')
    lines = open(setup_file, 'rt').readlines()
    pattern = r"version=['\"](.*?)['\"]"

    for line in lines:
        mo = re.search(pattern, line, re.M)
        if mo:
            return mo.group(1)

    raise RuntimeError(f"Unable to find version string in {setup_file}.")


if __name__ == "__main__":
    # Create tables (if not exist).
    for model in MODELS:
        model.create_table()

    # Run migration for this version.
    try:
        version = get_version().replace(".", "_")
        file_name = f"migration_{version}"
        spam = __import__(file_name, globals(), locals(), [], 0)
    except ModuleNotFoundError:
        pass  # no migration to execute
    except ProgrammingError:
        pass  # probably migration was already executed
