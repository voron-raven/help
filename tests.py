import json
import os
import unittest

# Set test DB before app init.
if os.environ.get('HELP_DB_NAME') is None:
    os.environ['HELP_DB_NAME'] = 'test_help'

from main import app  # noqa: E402
from models import Organization, Opportunity, MODELS, get_token  # noqa: E402

app.config['WTF_CSRF_ENABLED'] = False


class BaseTestCase(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        # Create all tables.
        for model in MODELS:
            model.create_table()

        cls.organization = Organization(logo="test", title="Test organization",
                                        site="mkeda.me", info="This a test",
                                        password="testtest")
        cls.organization.save()

        # authorization token
        cls.token = get_token({'id': cls.organization.id,
                               'title': cls.organization.title})

        cls.opportunity = Opportunity(
            title="Test org",
            category=1,
            organization_id=cls.organization.id,
            task="task",
            objectives=1,
            vol_needed=10,
            hours_per_week_min=1,
            hours_per_week_max=4,
            duration="1 week",
            requirements="We need something cool",
            region=2,
            language=1
        )
        cls.opportunity.save()

    @classmethod
    def tearDownClass(cls):
        # Drop all tables.
        for model in reversed(MODELS):
            model.drop_table(cascade=True)

    def test_opportunities_api(self):
        # POST
        _, response = app.test_client.post('/v1/', data=json.dumps({}))
        self.assertEqual(response.status, 400)  # empty post

        _, response = app.test_client.post('/v1/', data=json.dumps({
            "title": "test",
            "category": 2,
            "task": "task",
            "objectives": 1,
            "vol_needed": 1,
            "hours_per_week_min": 1,
            "hours_per_week_max": 4,
            "duration": "1 month",
            "requirements": "test",
            "region": 0,
            "language": 0
        }))
        self.assertEqual(response.status, 400)  # no authorization header

        _, response = app.test_client.post('/v1/', data=json.dumps({
            "title": "test",
            "category": 2,
            "task": "task",
            "objectives": 1,
            "vol_needed": 1,
            "hours_per_week_min": 1,
            "hours_per_week_max": 4,
            "duration": "1 month",
            "requirements": "test",
            "region": 0,
            "language": 0
        }), headers={'authorization': self.token})
        self.assertEqual(response.status, 201)

        # GET
        _, response = app.test_client.get('/v1/')
        self.assertEqual(response.status, 200)
        self.assertEqual(len(response.json), 2)

    def test_opportunity_api(self):
        # GET
        _, response = app.test_client.get('/v1/' + str(self.opportunity.id))
        self.assertEqual(response.status, 200)

        # PUT
        _, response = app.test_client.put('/v1/' + str(self.opportunity.id),
                                          data=json.dumps({
                                              "title": "test2",
                                          }))
        self.assertEqual(response.status, 400)  # no authorization header

        _, response = app.test_client.put(
            '/v1/' + str(self.opportunity.id),
            data=json.dumps({"title": "test2"}),
            headers={'authorization': self.token}
        )
        self.assertEqual(response.status, 200)  # success
        self.assertEqual(response.json['title'], "test2")

        _, response = app.test_client.put(
            '/v1/46',
            data=json.dumps({"title": "test2"}),
            headers={'authorization': self.token}
        )
        self.assertEqual(response.status, 404)  # id 46 doesn't exist

        # DELETE
        _, response = app.test_client.delete('/v1/' + str(self.opportunity.id))
        self.assertEqual(response.status, 400)  # no authorization header

        _, response = app.test_client.delete(
            '/v1/' + str(self.opportunity.id),
            headers={'authorization': self.token}
        )
        self.assertEqual(response.status, 204)
        opportunities = Opportunity.select().where(
            Opportunity.id == self.opportunity.id).execute()
        self.assertEqual(len(opportunities), 0)

    def test_login_api(self):
        # POST
        _, response = app.test_client.post('/v1/login', data=json.dumps({}))
        self.assertEqual(response.status, 400)  # empty post
        self.assertEqual(response.json['message'][0],
                         "Need to specify name and password!")

        _, response = app.test_client.post(
            '/v1/login',
            data=json.dumps({'title': self.organization.title,
                             'password': 'wrong password'})
        )
        self.assertEqual(response.status, 400)  # wrong password
        self.assertEqual(response.json['message'][0],
                         "Not valid title or password!")

        _, response = app.test_client.post(
            '/v1/login',
            data=json.dumps({'title': self.organization.title,
                             'password': self.organization.password}))
        self.assertEqual(response.status, 201)  # correct title and password
